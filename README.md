# Analyze, Detect and Harness Adversarial Attacks

Analyzujte základní principy adversariálních útoků, pak vytvořte model, který
bude schopen detekovat a eliminovat adversariální útoky.

Vyzkoušená řešení - Vyhlazení obrázků

## Pro spuštění:

Veškerá data jsou součástí repozitáře.

Pro spuštění budete potřebovat následující knihovny:
'''matplotlib, numpy, scikit-image, json, pickle'''

Pilotní experiment je v soubory main.ipynb. Testované detekční metody jsou
v soubory detection.py.

Generování dat je v souboru adversarial_sample_generation.ipynb. 
