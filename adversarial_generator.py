import numpy as np

#methods modified from https://github.com/dangeng/Simple_Adversarial_Examples

def generate_rubbish_sample(network, desired_label, steps, step_size):
    """Generates a rubbish adversarial sample for given network.
    
    :param network: neural network trained for classification
    :type network: Network
    :param desired_label: desired label of adversarial sample
    :type desired_label: int
    :param steps: number of steps to generate adversarial sample
    :type steps: int
    :param step_size: gradient descent step size
    :type step_size: float
    :return: adversarial sample
    :rtype: np.ndarray
    """
    
    # generate random image
    adversarial_sample = np.random.normal(.5, .3, (784, 1))
    
    # modify image to desired adversarial sample
    for _ in range(steps):
        derivative = network.input_derivative(adversarial_sample, desired_label)
        adversarial_sample -= step_size * derivative
    
    return adversarial_sample

def sneaky_adversarial(network, data, desired_label, source_label, steps, step_size, lam = 0.05):
    """Generate adversarial sample that looks as a proper image of different class to the
    human eye. Take a random sample of given source label and modifies it to be classified
    ass desired label.
    
    :param network: trained neural network
    :type network: Network
    :param data: sample_data and sample_labels in a zip
    :type data: zip
    :param desired_label: desired label of adversarial sample
    :type desired_label: int
    :param source_label: source label of image we want to modify
    :type source_label: int
    :param steps: number of steps to generate adversarial sample
    :type steps: int
    :param step_size: gradient descent step size
    :type step_size: float
    :param lam: regularization parameter, defaults to 0.05
    :type lam: float, optional
    :return: adversarial sample
    :rtype: np.ndarray
    """
    samples, labels = data
    
    # Find random instance of m in test set
    idx = np.random.randint(0, 0.5*len(samples))
    while labels[idx] != source_label:
        idx += 1
    
    # Set the goal output
    goal = np.zeros((10,1))
    goal[desired_label] = 1

    # Create a random image to initialize gradient descent with
    adversarial_sample = np.random.normal(.5, .3, (784, 1))

    # Gradient descent on the input
    for _ in range(steps):
        # Calculate the derivative
        d = network.input_derivative(adversarial_sample, goal)
        
        # The GD update on x, with an added penalty to the cost function
        adversarial_sample -= step_size * (d + lam * (adversarial_sample - samples[idx]))

    for i in range(len(adversarial_sample)):
        if adversarial_sample[i] > 1:
            adversarial_sample[i] = 1.0
        elif adversarial_sample[i] < -1:
            adversarial_sample = -1

    return adversarial_sample