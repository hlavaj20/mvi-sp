import numpy as np
import skimage
import skimage.morphology

#method modified from https://github.com/dangeng/Simple_Adversarial_Examples
def binary_threshold(sample):
    """Round pixel values to closest value.
    
    :param sample: pixel representation of image
    :type sample: np.ndarray
    :return: modified sample image
    :rtype: np.ndarray
    """
    sample = (sample > 0.5).astype(float)
    return sample

def smooth_image(sample, shape, disk_size=3):
    """Smoothes images by mean filter of size of disk.
    
    :param sample: sample image to be smoothed as 1D array
    :type sample: np.ndarray
    :param shape: shape of the original image
    :type shape: tuple
    :param disk_size: diameter of smoothing filter, defaults to 3
    :type disk_size: int, optional
    :return: smoothed sample image as 1D array
    :rtype: np.ndarray
    """
    image = skimage.img_as_ubyte(sample.reshape(shape))
    image =  skimage.filters.rank.mean(image, skimage.morphology.disk(disk_size))
    return skimage.img_as_float64(image.reshape(shape[0]*shape[1], 1))